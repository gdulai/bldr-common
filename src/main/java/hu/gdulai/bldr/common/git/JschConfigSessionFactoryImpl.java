package hu.gdulai.bldr.common.git;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import org.eclipse.jgit.transport.JschConfigSessionFactory;
import org.eclipse.jgit.transport.OpenSshConfig.Host;
import org.eclipse.jgit.util.FS;

/** */
public class JschConfigSessionFactoryImpl extends JschConfigSessionFactory {

  @Nullable private String path;
  @Nullable private String passPharse;

  private JschConfigSessionFactoryImpl(@Nonnull String path, @Nullable String passPharse) {
    this.path = path;
    this.passPharse = passPharse;
  }

  @Override
  protected JSch createDefaultJSch(FS fs) throws JSchException {
    JSch jSch = super.createDefaultJSch(fs);
    if (passPharse != null) jSch.addIdentity(path, passPharse);
    else if (path != null) jSch.addIdentity(path);
    return jSch;
  }

  public static JschConfigSessionFactoryImpl of() {
    return of(null, null);
  }

  public static JschConfigSessionFactoryImpl of(@Nonnull String path) {
    return of(path, null);
  }

  public static JschConfigSessionFactoryImpl of(@Nonnull String path, @Nullable String passPharse) {
    return new JschConfigSessionFactoryImpl(path, passPharse);
  }

  @Override
  protected void configure(Host hc, Session session) {
    session.setConfig("StrictHostKeyChecking", "no");
  }
}