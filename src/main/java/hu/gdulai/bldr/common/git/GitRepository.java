package hu.gdulai.bldr.common.git;

import static hu.gdulai.bldr.common.Constants.BLDR_ERR_TAG;

import hu.gdulai.bldr.common.repo.LocalRepository;
import java.nio.file.Path;
import java.text.MessageFormat;
import java.util.Map;
import java.util.Optional;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import org.eclipse.jgit.api.CloneCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.LsRemoteCommand;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.transport.SshSessionFactory;
import org.eclipse.jgit.transport.SshTransport;
import org.gradle.api.logging.Logger;

/** */
public class GitRepository {

  private static final SshSessionFactory defltSessFactory = JschConfigSessionFactoryImpl.of();

  @Nonnull private final String url;
  @Nonnull private final String branch;
  @Nullable private final SshSessionFactory sessFactory;
  @Nonnull private final Logger logger;
  @Nullable private String revId;
  @Nullable private String repoName;
  @Nullable private Repository gitRepo;

  public GitRepository(@Nonnull String url, @Nonnull String branch, @Nonnull Logger logger) {
    this.url = url;
    this.branch = branch;
    this.sessFactory = null;
    this.logger = logger;
  }

  public GitRepository(
      @Nonnull String url,
      @Nonnull String branch,
      @Nonnull SshSessionFactory sessFactory,
      @Nonnull Logger logger) {
    this.url = url;
    this.branch = branch;
    this.sessFactory = sessFactory;
    this.logger = logger;
  }

  /**
   * Clones the repository into the FileSystem.
   *
   * <p>Returns Optional of the Path of the clone command or an empty Optional.
   */
  public Optional<Path> clone() {
    final CloneCommand cloneCmd =
        Git.cloneRepository()
            .setTransportConfigCallback(
                transport -> {
                  if (transport instanceof SshTransport sshTransport)
                    sshTransport.setSshSessionFactory(
                        sessFactory != null ? sessFactory : defltSessFactory);
                });

    try {
      Path dir = LocalRepository.getInstance().resolveSources(getRepoName());
      gitRepo =
          cloneCmd.setURI(url).setBranch(branch).setDirectory(dir.toFile()).call().getRepository();

      return Optional.of(dir);
    } catch (GitAPIException e) {
      logger.error(MessageFormat.format("{0} Failed to clone repository!", BLDR_ERR_TAG), e);
      return Optional.empty();
    }
  }

  /** Returns true if the branch is already built, false if not or an error occurred. */
  public boolean alreadyBuilt() {
    LsRemoteCommand lsRemoteCmd =
        Git.lsRemoteRepository()
            .setTransportConfigCallback(
                transport -> {
                  if (transport instanceof SshTransport sshTransport)
                    sshTransport.setSshSessionFactory(
                        sessFactory != null ? sessFactory : defltSessFactory);
                });
    try {
      Map<String, Ref> res = lsRemoteCmd.setRemote(url).callAsMap();
      Ref branchRef = res.get("refs/heads/" + branch);
      if (branchRef == null)
        throw new IllegalStateException(
            MessageFormat.format("No ref data found for {0}-{1}!", url, branch));

      revId = branchRef.getObjectId().getName();
      return LocalRepository.getInstance().isRevisionBuilt(url, branch, revId);
    } catch (GitAPIException e) {
      logger.error(MessageFormat.format("{0} Failed to ls remote repository!", BLDR_ERR_TAG), e);
      return false;
    }
  }

  public String getRepoName() {
    if (repoName == null) {
      int start = url.lastIndexOf("/");
      int end = url.lastIndexOf(".");
      repoName = url.substring(start + 1, end);
    }
    return repoName;
  }

  @Nonnull
  public String getUrl() {
    return url;
  }

  @Nonnull
  public String getBranch() {
    return branch;
  }

  @Nonnull
  public String getRevId() {
    if (revId != null) return revId;
    if (gitRepo == null) throw new IllegalStateException("Git repository was not initialized yet!");
    throw new IllegalStateException("Revision id is null!");
  }
}
