package hu.gdulai.bldr.common.project;

import static hu.gdulai.bldr.common.Constants.BLDR_ERR_TAG;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.MessageFormat;
import java.util.List;
import java.util.stream.Stream;
import javax.annotation.Nonnull;
import org.gradle.api.logging.Logger;

/** */
public class BuildSystemProjectFactory {

  public static Stream<BuildSystemProject> createProject(
      @Nonnull Logger logger, @Nonnull Path projectPath) {

    Path gradleBuildFile = projectPath.resolve("build.gradle");
    if (Files.exists(gradleBuildFile)) return Stream.of(new GradleProject(logger, projectPath));

    Path settingsGradleFile = projectPath.resolve("settings.gradle");
    if (Files.exists(settingsGradleFile)) {
      try {
        List<String> lines = Files.readAllLines(settingsGradleFile, Charset.forName("UTF-8"));
        return lines.stream()
            .filter(s -> s.startsWith("include"))
            .map(
                s -> {
                  int includeStart = s.indexOf("(");
                  if (includeStart == -1) return null;

                  int includeEnd = s.indexOf(")");
                  if (includeEnd == -1) return null;

                  String subPath =
                      s.substring(includeStart + 1, includeEnd).replace("'", "").replace("\"", "");
                  return new GradleProject(logger, projectPath.resolve(subPath));
                });
      } catch (IOException e) {
        e.printStackTrace();
      }
    }

    Path mavenPomFile = projectPath.resolve("pom.xml");
    if (Files.exists(mavenPomFile)) return Stream.of(new MavenProject());

    throw new IllegalStateException(
        MessageFormat.format("{0} Could not determine project type!", BLDR_ERR_TAG));
  }
}
