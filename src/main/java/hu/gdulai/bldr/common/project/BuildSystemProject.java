package hu.gdulai.bldr.common.project;

import java.nio.file.Path;
import java.util.Optional;
import javax.annotation.Nonnull;

/** */
public interface BuildSystemProject {

  /**
   * Builds the build system project(eg.: Gradle, Maven...etc. based on the implementation) and
   * returns the path of the build result.
   *
   * @return {@link Path} of the built resource.
   */
  @Nonnull
  Optional<Path> build(@Nonnull String... args);

  /** Removes the project folder. */
  void removeProjectFolder();
}
