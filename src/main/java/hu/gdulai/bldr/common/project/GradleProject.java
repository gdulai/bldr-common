package hu.gdulai.bldr.common.project;

import static hu.gdulai.bldr.common.Constants.BLDR_ERR_TAG;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.MessageFormat;
import java.util.Optional;
import javax.annotation.Nonnull;
import org.apache.commons.io.FileUtils;
import org.gradle.api.logging.Logger;
import org.gradle.tooling.*;

/** */
public class GradleProject implements BuildSystemProject {

  private final Logger logger;
  private final Path projectPath;

  GradleProject(@Nonnull Logger logger, @Nonnull Path projectPath) {
    this.logger = logger;
    this.projectPath = projectPath;
  }

  @Nonnull
  @Override
  public Optional<Path> build(@Nonnull String... args) {
    GradleConnector connector = GradleConnector.newConnector();
    ProjectConnection projectConnection =
        connector.forProjectDirectory(projectPath.toFile()).connect();
    BuildLauncher build = projectConnection.newBuild();
    try {
      build.forTasks("clean", "assemble").run();
      Path libsPath = projectPath.resolve("build").resolve("libs");
      if (!Files.exists(libsPath))
        logger.error(
            MessageFormat.format(
                "{0} No build/libs path found for: {1}", BLDR_ERR_TAG, projectPath));

      File libsDirectory = libsPath.toFile();
      File[] files = libsDirectory.listFiles();
      for (File file : files)
        if (file.getName().endsWith(".jar")) return Optional.of(file.toPath());

      return Optional.of(projectPath.resolve("build").resolve("libs"));
    } catch (BuildException e) {
      logger.error(
          MessageFormat.format("{0} Build failed project {1}:\n{2}", BLDR_ERR_TAG, projectPath, e));
      removeProjectFolder();
      return Optional.empty();
    }
  }

  @Override
  public void removeProjectFolder() {
    try {
      FileUtils.deleteDirectory(projectPath.toFile());
    } catch (IOException ex) {
      logger.error(
          MessageFormat.format(
              "{0} Failed to delete project path: {1}", BLDR_ERR_TAG, projectPath));
    }
  }
}
