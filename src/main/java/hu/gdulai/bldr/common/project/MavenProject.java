package hu.gdulai.bldr.common.project;

import java.nio.file.Path;
import java.util.Optional;
import javax.annotation.Nonnull;

/** */
public class MavenProject implements BuildSystemProject {

  @Nonnull
  @Override
  public Optional<Path> build(@Nonnull String... args) {
    return Optional.empty();
  }

  @Override
  public void removeProjectFolder() {}
}
