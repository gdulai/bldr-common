package hu.gdulai.bldr.common;

public class Constants {

  public static final String BLDR_TAG = "[BLDR]";
  public static final String BLDR_ERR_TAG = "[BLDR-ERR]";
}
