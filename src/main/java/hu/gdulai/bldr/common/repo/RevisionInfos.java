package hu.gdulai.bldr.common.repo;

import java.io.Serializable;
import java.util.List;

/** */
public class RevisionInfos implements Serializable {

  private List<RevisionInfo> revisionInfos;

  public RevisionInfos() {}

  public RevisionInfos(List<RevisionInfo> revisionInfos) {
    this.revisionInfos = revisionInfos;
  }

  public void setRevisionInfos(List<RevisionInfo> revisionInfos) {
    this.revisionInfos = revisionInfos;
  }

  public List<RevisionInfo> getRevisionInfos() {
    return revisionInfos;
  }
}
