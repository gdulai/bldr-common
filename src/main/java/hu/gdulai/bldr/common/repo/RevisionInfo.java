package hu.gdulai.bldr.common.repo;

import java.io.Serializable;
import java.util.Objects;
import javax.annotation.Nonnull;

/** */
public class RevisionInfo implements Serializable {

  @Nonnull private String repoUrl;
  @Nonnull private String branch;
  @Nonnull private String revisionId;
  @Nonnull private String jarPath;

  public RevisionInfo(
      @Nonnull String repoUrl,
      @Nonnull String branch,
      @Nonnull String revisionId,
      @Nonnull String jarPath) {
    this.repoUrl = repoUrl;
    this.branch = branch;
    this.revisionId = revisionId;
    this.jarPath = jarPath;
  }

  public String getRepoUrl() {
    return repoUrl;
  }

  public void setRepoUrl(String repoUrl) {
    this.repoUrl = repoUrl;
  }

  public String getBranch() {
    return branch;
  }

  public void setBranch(String branch) {
    this.branch = branch;
  }

  public String getRevisionId() {
    return revisionId;
  }

  public void setRevisionId(String revisionId) {
    this.revisionId = revisionId;
  }

  public String getJarPath() {
    return jarPath;
  }

  public void setJarPath(String jarPath) {
    this.jarPath = jarPath;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    RevisionInfo that = (RevisionInfo) o;
    return Objects.equals(repoUrl, that.repoUrl)
        && Objects.equals(branch, that.branch)
        && Objects.equals(revisionId, that.revisionId)
        && Objects.equals(jarPath, that.jarPath);
  }

  @Override
  public int hashCode() {
    return Objects.hash(repoUrl, branch, revisionId, jarPath);
  }
}
