package hu.gdulai.bldr.common.repo;

import java.io.Serializable;
import javax.annotation.Nonnull;

public class GradleDependency implements Serializable {

  @Nonnull private final String configuration;
  @Nonnull private final String group;
  @Nonnull private final String name;
  @Nonnull private final String version;

  public GradleDependency(
      @Nonnull String configuration,
      @Nonnull String group,
      @Nonnull String name,
      @Nonnull String version) {
    this.configuration = configuration;
    this.group = group;
    this.name = name;
    this.version = version;
  }

  public String getConfiguration() {
    return configuration;
  }

  public String getGroup() {
    return group;
  }

  public String getName() {
    return name;
  }

  public String getVersion() {
    return version;
  }
}
