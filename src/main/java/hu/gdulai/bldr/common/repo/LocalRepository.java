package hu.gdulai.bldr.common.repo;

import static hu.gdulai.bldr.common.Constants.BLDR_TAG;

import com.google.gson.Gson;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.MessageFormat;
import java.util.*;
import java.util.stream.Collectors;
import javax.annotation.Nonnull;
import org.apache.commons.io.FileUtils;
import org.eclipse.jgit.util.FS;

/**
 * Class wrapping the functionality and handling of the ~/.bldr folder which stores information for
 * the plugin.
 */
public class LocalRepository {

  private static LocalRepository instance;

  public static LocalRepository getInstance() {
    if (instance == null) {
      try {
        instance = new LocalRepository();
      } catch (IOException e) {
        throw new IllegalStateException(
            MessageFormat.format("{0} Could not init local repository!", BLDR_TAG), e);
      }
    }
    return instance;
  }

  public static final void disposeInstance() {
    instance = null;
  }

  /** Root path of the .bldr folder. */
  private final Path repositoryRoot;

  /** Path of the sources folder where the repos are temporarily stored. */
  private final Path repositorySources;

  /** Path of the store folder which contains the built JARs. */
  private final Path repositoryStore;

  /** Stores the RevisionInfo(which url/branch was built with revisionId) by the url. */
  private Map<String, List<RevisionInfo>> revisionInfos;

  /** Creates all the technical folders, files and loads the existing technical information.. */
  private LocalRepository() throws IOException {
    repositoryRoot = FS.detect().userHome().toPath().resolve(".bldr");
    if (!Files.exists(repositoryRoot)) Files.createDirectories(repositoryRoot);

    repositorySources = repositoryRoot.resolve("sources");
    if (!Files.exists(repositorySources)) Files.createDirectories(repositorySources);

    repositoryStore = repositoryRoot.resolve("store");
    if (!Files.exists(repositoryStore)) Files.createDirectories(repositoryStore);

    Path stateJsonPath = repositoryRoot.resolve("state.json");
    if (!Files.exists(stateJsonPath)) Files.createFile(stateJsonPath);

    FileReader reader = new FileReader(repositoryRoot.resolve("state.json").toFile());
    RevisionInfos revInfos = new Gson().fromJson(reader, RevisionInfos.class);
    List<RevisionInfo> tempRevisionInfos = revInfos == null ? null : revInfos.getRevisionInfos();
    reader.close();

    revisionInfos = new HashMap<>();
    if (tempRevisionInfos != null) {
      for (RevisionInfo revInfo : tempRevisionInfos) {
        List<RevisionInfo> infos = revisionInfos.get(revInfo.getRepoUrl());
        if (infos == null) {
          infos = new ArrayList<>();
          revisionInfos.put(revInfo.getRepoUrl(), infos);
        }
        infos.add(revInfo);
      }
    }
  }

  public Path resolveSources(@Nonnull String name) {
    return repositorySources.resolve(name);
  }

  /**
   * Adds the built jar to the store folder. Stores the information in the state.json also.
   *
   * @param url of the repo which the jar was built from.
   * @param branch branch that is currently checked out.
   * @param revId the revision id the jar was built from.
   * @param jarPath the path of the built jar.
   * @throws IOException if the jar move is failed.
   */
  public void addToStore(
      @Nonnull String url, @Nonnull String branch, @Nonnull String revId, @Nonnull Path jarPath)
      throws IOException {
    FileUtils.copyFileToDirectory(jarPath.toFile(), repositoryStore.toFile());
    List<RevisionInfo> revInfos = revisionInfos.get(url);
    if (revInfos == null) {
      revInfos = new ArrayList<>();
      revisionInfos.put(url, revInfos);
    }
    RevisionInfo revInfo = new RevisionInfo(url, branch, revId, jarPath.toString());
    revInfos.add(revInfo);
  }

  /**
   * @param url of the repo which the jar was built from.
   * @param branch branch that is currently checked out.
   * @param revId the revision id the jar was built from.
   * @return TRUE if the revision is already built.
   */
  public boolean isRevisionBuilt(
      @Nonnull String url, @Nonnull String branch, @Nonnull String revId) {
    List<RevisionInfo> revInfos = revisionInfos.get(url);
    if (revInfos == null) return false;
    return revInfos.stream()
        .anyMatch(
            revInfo ->
                revInfo.getRepoUrl().equals(url)
                    && revInfo.getBranch().equals(branch)
                    && revInfo.getRevisionId().equals(revId));
  }

  /** Saves the store information. */
  public final void saveInfo() throws IOException {
    FileWriter writer = new FileWriter(repositoryRoot.resolve("state.json").toFile());
    writer.write(
        new Gson()
            .toJson(
                new RevisionInfos(
                    revisionInfos.values().stream()
                        .flatMap(Collection::stream)
                        .collect(Collectors.toList()))));
    writer.close();
  }

  public final Path getRepositoryStore() {
    return repositoryStore;
  }
}
